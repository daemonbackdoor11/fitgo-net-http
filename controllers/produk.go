package controllers

import (
	"context"
	"encoding/json"
	"fitgo/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"

	//. "fitgo/views"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"net/http"
)

// https://medium.com/glottery/golang-and-mongodb-with-go-mongo-driver-part-1-1c43aba25a1
// https://medium.com/@faygun89/create-rest-api-with-golang-and-mongodb-d38d2e1d9714
type Produk struct {
	Id         primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Nama       string             `json:"nama" bson:"nama,omitempty"`
	Keterangan string             `json:"keterangan" bson:"keterangan,omitempty"`
	Harga      int                `json:"harga" bson:"harga,omitempty"`
	CreatedAt  time.Time          `bson:"created_at" json:"created_at,omitempty"`
	UpdateAt   time.Time          `bson:"updated_at" json:"updated_at,omitempty"`
}

func IndexProduk(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	produk, err := models.DB.Collection("produk").Find(context.TODO(), bson.D{{}})
	if err != nil {

	}
	res := []Produk{}
	for produk.Next(context.Background()) {
		result := Produk{}
		err := produk.Decode(&result)
		if err != nil {
			log.Fatal(err)
		}
		res = append(res, result)
	}
	json.NewEncoder(w).Encode(&res)
}

func AddProduk(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	produk := Produk{}
	err := json.NewDecoder(r.Body).Decode(&produk)
	if err != nil {
		log.Println(err)
	}
	ctx := context.Background()
	insert, err := models.DB.Collection("produk").InsertOne(ctx, produk)
	if insert == nil {
		log.Println(insert)
	}
	log.Print("monggo err : ", err)
	e := json.NewEncoder(w).Encode(&insert)
	if e != nil {
		log.Println(err)
	}
}

func DelProduk(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	delete, _ := models.DB.Collection("produk").DeleteOne(context.Background(), bson.D{{"_id", id}})
	defer json.NewEncoder(w).Encode(&delete)
}

// https://www.mongodb.com/blog/post/mongodb-go-driver-tutorial#UpdateDocument
func UpdateProduk(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil {
		log.Println(err)
	}
	produk := Produk{}
	json.NewDecoder(r.Body).Decode(&produk)
	update, _ := models.DB.Collection("produk").UpdateOne(context.TODO(), bson.D{{"_id", id}}, bson.D{
		{"$set", bson.D{
			{"nama", produk.Nama},
			{"keterangan", produk.Keterangan},
			{"harga", produk.Harga},
		},
		}})
	json.NewEncoder(w).Encode(update)
}

func ShowProduk(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	show := models.DB.Collection("produk").FindOne(context.TODO(), bson.D{{"_id", id}})
	if show == nil {
		json.NewEncoder(w).Encode(nil)
	}
	produk := Produk{}
	show.Decode(&produk)
	json.NewEncoder(w).Encode(&produk)
}
