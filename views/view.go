package views

import (
	"html/template"
	"log"
	"net/http"
)

type View struct {
	Response http.ResponseWriter
	Path     string
	Data     interface{}
	Template string
}

func Render(v View) error {
	t := template.New("New")
	t, err := t.ParseFiles("./views/template/"+v.Path+".html", "./views/template/"+v.Template+".html")
	if err != nil {
		log.Fatal("[!] Views error , check the template or layout ! %s", err)
	}
	return t.ExecuteTemplate(v.Response, "app", v.Data)
}
